package com.myspace;

/**
 * This class was automatically generated by the data modeler tool.
 */

public class Notificacion implements java.io.Serializable {

	static final long serialVersionUID = 1L;

	@org.kie.api.definition.type.Label(value = "total")
	private java.lang.Integer total;

	public Notificacion() {
	}

	public java.lang.Integer getTotal() {
		return this.total;
	}

	public void setTotal(java.lang.Integer total) {
		this.total = total;
	}

	public Notificacion(java.lang.Integer total) {
		this.total = total;
	}

}